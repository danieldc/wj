# wj: a simple work journal tool

`wj` is a simple work journal tool.


## Usage

`wj some text` will write an entry to the work journal file.

Entries in the wj.txt file have the following format: `YYYY-MM-dd.hh:mm message content`

The work journal file location is `~/todo/wj.txt`.
