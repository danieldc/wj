package main

import (
	"fmt"
	"os"
	"time"
)

func main() {
	if len(os.Args) == 1 || (len(os.Args) == 2 && (os.Args[1] == "-h" || os.Args[1] == "--help")) {
		fmt.Println("wj: a simple work journal tool, writing entries to ~/todo/wj.txt")
		fmt.Print("Usage: wj some text")
		return
	}

	msg := fmt.Sprintf(time.Now().Format("2006-01-02.15:04"))
	for _, v := range os.Args[1:] {
		msg = fmt.Sprintf("%s %s", msg, v)
	}
	msg = msg + "\n"

	homeDir, err := os.UserHomeDir()
	if err != nil {
		panic(err)
	}

	f, err := os.OpenFile(homeDir+"/todo/wj.txt", os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0600)
	if err != nil {
		panic(err)
	}
	defer f.Close()

	if _, err = f.WriteString(msg); err != nil {
		panic(err)
	}
}
